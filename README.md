# OpenStreetMap link for Google Search results

In the **European Union** Google changed it's search results in march 2024. If you search for a geographical location, the little map that is shown with the search results no longer is a link to Google Maps results.

Since it's very convenient to have a link to a bigger map, I ([Michel Stuyts](https://stuyts.xyz)) created a **userscript** that adds a link to OpenStreetMap results. It is still a very early version, so it's very probable it doesn't work for everyone, because there are very different versions of Google Search results.  Try searching for the same location on https://www.google.com and also on https://www.google.be and the results pages look very different.  This is the reason the effect of this userscript is different on different versions of google Search results. One possible effect is that the small map in the top of the results is a link to **OpenstreetMap**.  The other possible result is an extra link to OpenStreetMap next to the Wikipedia result that is also on the Search Results page. Or both at the same time off course. What the effect is outside the European Union? I don't know.  If you find out, just let me know.

If this userscript doesn't work for you, you can add a bug report.  If you do so, please add the full page source of your Google Results to the report.

You can use the userscript in different **addons** in different browsers.  I personally use Tampermonkey in Firefox, but if you search for "userscripts" in your browser addons repository, you can find multiple addons to use userscripts with.  So you can just find one you like.

If you have installed one of the addons, you can open the following [link](https://gitlab.com/GIS-projects/openstreetmap-link-for-google-search-results/-/raw/main/OpenStreetMap_In_Google_Search.user.js?ref_type=heads) to **install** the userscript.


**Result 1**  
![Printscreen of linked map](https://gitlab.com/GIS-projects/openstreetmap-link-for-google-search-results/-/raw/main/images/userscript.png)


**Result 2**  
![Printscreen of link next to wikipedia link](https://gitlab.com/GIS-projects/openstreetmap-link-for-google-search-results/-/raw/main/images/userscript2.png)




